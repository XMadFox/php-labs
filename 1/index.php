<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
  <title>Лазарев 211-329</title>
</head>

<body>
  <header class="p-4 bg-orange-700 text-white h-16">
    <nav class="flex">
      <a href="/"><img alt="Лого поликека" src="https://mospolytech.ru/local/templates/main/dist/img/logos/mospolytech-logo-white.png" height="36px" width="36px" /></a>
      <h1 class="mx-auto text-xl">Hello, World!</h1>
    </nav>
  </header>
  <main class="h-[calc(100vh-7rem)]">
    <?php
    $url = 'https://madfox.laserflare.net/ru';
    echo "<iframe src='{$url}' width='full' class='w-full h-full'></iframe>";
    ?>
  </main>
  <footer class="p-2 h-12 bg-orange-700 text-white text-center">
    <p>Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг.</p>
  </footer>
</body>

</html>
